extends Node2D

# Variables
onready var animator = $Sprite/AnimationPlayer

export(float) var moveSpeed

var velocity = Vector2()
var moving = false

func _ready():
	
	pass

func _process(delta):
	Movement(delta)
	pass

func Movement(delta):
	if Input.is_action_pressed("left"):
		velocity = Vector2(-moveSpeed, 0) * delta
		if !animator.is_playing():
			animator.play("CactusWalkLeft")
	elif Input.is_action_pressed("right"):
		velocity = Vector2(moveSpeed, 0) * delta
		if !animator.is_playing():
			animator.play("CactusWalkRight")
	elif Input.is_action_pressed("down"):
		velocity = Vector2(0, moveSpeed) * delta
		if !animator.is_playing():
			animator.play("CactusWalkDown")
	elif Input.is_action_pressed("up"):
		velocity = Vector2(0, -moveSpeed) * delta
		if !animator.is_playing():
			animator.play("CactusWalkUp")
	else:
		animator.stop()
		velocity = Vector2(0, 0)
	
	position += velocity